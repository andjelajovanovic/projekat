import flask
from flask import Blueprint

from flask_jwt_extended import jwt_required

from utils.db import mysql

kupovina_blueprint = Blueprint("kupovina_blueprint", __name__)

@kupovina_blueprint.route("")
@jwt_required()
def get_all_kupovina():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupovina")
    kupovina = cursor.fetchall()
    
    for i in range(len(kupovina)):
        kupovina[i]["cena"] = float(kupovina[i]["cena"])
        if 'datum_kupovine' not in kupovina[i].keys():
            kupovina[i]['datum_kupovine'] = None 
            
    cursor.execute("SELECT * FROM korisnik")
    korisnik = cursor.fetchall()
            
    cursor.execute("SELECT * FROM nekretnina")
    nekretnina = cursor.fetchall()
    
    for i in range(len(nekretnina)):
        nekretnina[i]["cena"] = float(nekretnina[i]["cena"])

    return {1:korisnik, 2:nekretnina, 3:kupovina}

@kupovina_blueprint.route("<int:kupovina_id>")
def get_kupovina(kupovina_id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupovina WHERE id=%s", (kupovina_id,))
    kupovina = cursor.fetchone()
    if kupovina is not None:
        kupovina["cena"] = float(kupovina["cena"])
        return flask.jsonify(kupovina)
    return "", 404

@kupovina_blueprint.route("", methods=["POST"])
def dodavanje_kupovina():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO kupovina(datum_kupovine, cena, nekretnina_id, korisnik_id) VALUES(%(datum_kupovine)s, %(cena)s, %(nekretnina_id)s, %(korisnik_id)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@kupovina_blueprint.route("<int:kupovina_id>", methods=["PUT"])
def izmeni_kupovina(kupovina_id):
    kupovina = dict(flask.request.json)
    kupovina["kupovina_id"] = kupovina_id
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE kupovina SET datum_kupovine=%(datum_kupovine)s, cena=%(cena)s, nekretnina_id=%(nekretnina_id)s, kupovina_id=%(kupovina_id)s WHERE id=%(kupovina_id)s", kupovina)
    db.commit()
    cursor.execute("SELECT * FROM kupovina WHERE id=%s", (kupovina_id, ))
    kupovina = cursor.fetchone()
    kupovina["cena"] = float(kupovina["cena"])
    return flask.jsonify(kupovina)

@kupovina_blueprint.route("<int:kupovina_id>", methods=["ukloni"])
def ukloni_kupovina(kupovina_id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM kupovina WHERE id=%s", (kupovina_id, ))
    db.commit()
    return ""

@kupovina_blueprint.route("pretraga", endpoint='pretraga', methods=["POST"],)
@jwt_required()
def pretraga():
    objekat = flask.request.json
    tekst = "SELECT * FROM kupovina WHERE "
    for key, value in objekat.items():
        tekst += "{}='{}' AND ".format(key, value)

    tekst = tekst[0:-4]
    cursor = mysql.get_db().cursor()
    cursor.execute(tekst)
    kupovina = cursor.fetchall()
    for i in range(len(kupovina)):
        kupovina[i]["cena"] = float(kupovina[i]["cena"])
    return flask.jsonify(kupovina)