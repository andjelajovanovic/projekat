from functools import wraps
import flask
from flask import Blueprint
from flask.json import jsonify

from flask_jwt_extended import jwt_required
from flask_jwt_extended.utils import get_jwt
from flask_jwt_extended.view_decorators import verify_jwt_in_request

from utils.db import mysql

korisnik_blueprint = Blueprint("korisnik_blueprint", __name__)

def is_admin():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            verify_jwt_in_request()
            claims = get_jwt()
            if claims["roles"][0] == 1:
                return fn(*args, **kwargs)
            else: 
                return jsonify(msg="Dozvoljeno samo administratorima!"), 403

        return decorator

    return wrapper

@korisnik_blueprint.route("", methods=["GET"], endpoint='get_all_korisnik')
@jwt_required()
@is_admin()
def get_all_korisnik():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnik")
    korisnik = cursor.fetchall()
    
    cursor.execute("SELECT * FROM uloge")
    uloge = cursor.fetchall()
    return {1:korisnik, 2:uloge}

@korisnik_blueprint.route("<int:korisnik_id>", methods=["GET"], endpoint="get_korisnik")
@is_admin()
def get_korisnik(korisnik_id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnik WHERE id=%s", (korisnik_id,))
    korisnik = cursor.fetchone()
    if korisnik is not None:
        return flask.jsonify(korisnik)
    
    return "", 404

@korisnik_blueprint.route("", methods=["POST"], endpoint="dodavanje_korisnik")
@is_admin()
def dodavanje_korisnik():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO korisnik(ime, prezime, email, lozinka, uloge_id) VALUES(%(ime)s, %(prezime)s, %(email)s, %(lozinka)s, %(uloge_id)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@korisnik_blueprint.route("<int:korisnik_id>", methods=["PUT"], endpoint="izmeni_korisnik")
@is_admin()
def izmeni_korisnik(korisnik_id):
    korisnik = dict(flask.request.json)
    korisnik["korisnik_id"] = korisnik_id
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE korisnik SET ime=%(ime)s, prezime=%(prezime)s, email=%(email)s, lozinka=%(lozinka)s, uloge_id=%(uloge_id)s WHERE id=%(korisnik_id)s", korisnik)
    db.commit()
    cursor.execute("SELECT * FROM korisnik WHERE id=%s", (korisnik_id,))
    korisnik = cursor.fetchone()
    return flask.jsonify(korisnik)

@korisnik_blueprint.route("<int:korisnik_id>", methods=["delete"], endpoint="ukloni_korisnik")
@is_admin()
def ukloni_korisnik(korisnik_id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("delete FROM korisnik WHERE id=%s", (korisnik_id, ))
    db.commit()
    return ""

@korisnik_blueprint.route("pretraga", endpoint='pretraga', methods=["POST"],)
@jwt_required()
def pretraga():
    objekat = flask.request.json
    tekst = "SELECT * FROM korisnik WHERE "
    for key, value in objekat.items():
        tekst += "{}='{}' AND ".format(key, value)

    tekst = tekst[0:-4]
    cursor = mysql.get_db().cursor()
    cursor.execute(tekst)
    korisnik = cursor.fetchall()
    return flask.jsonify(korisnik)
