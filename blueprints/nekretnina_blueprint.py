from functools import wraps
import flask
from flask import Blueprint
from flask.json import jsonify

from flask_jwt_extended import jwt_required
from flask_jwt_extended.utils import get_jwt
from flask_jwt_extended.view_decorators import verify_jwt_in_request

from utils.db import mysql

nekretnina_blueprint = Blueprint("nekretnina_blueprint", __name__)

def is_admin():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            verify_jwt_in_request()
            claims = get_jwt()
            if claims["roles"][0] == 1:
                return fn(*args, **kwargs)
            else: 
                return jsonify(msg="Dozvoljeno samo administratorima!"), 403

        return decorator

    return wrapper

@nekretnina_blueprint.route("", methods=["GET"], endpoint='get_all_nekretnina')
@jwt_required()
@is_admin()
def get_all_nekretnina():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM nekretnina")
    nekretnina = cursor.fetchall()
    for p in nekretnina:
        p["cena"] = float(p["cena"])
    return flask.jsonify(nekretnina)

@nekretnina_blueprint.route("<int:nekretnina_id>")
@is_admin()
def get_nekretnina(nekretnina_id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM nekretnina WHERE id=%s", (nekretnina_id,))
    nekretnina = cursor.fetchone()
    if nekretnina is not None:
        nekretnina["cena"] = float(nekretnina["cena"])
        return flask.jsonify(nekretnina)
    
    return "", 404

@nekretnina_blueprint.route("", methods=["POST"])
@is_admin()
def dodavanje_nekretnina():
    db = mysql.get_db() 
    print("test")
    cursor = db.cursor()
    cursor.execute("INSERT INTO nekretnina(naziv, adresa, cena) VALUES(%(naziv)s, %(adresa)s, %(cena)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@nekretnina_blueprint.route("<int:nekretnina_id>", methods=["PUT"])
@is_admin()
def izmeni_nekretnina(nekretnina_id):
    nekretnina = dict(flask.request.json)
    nekretnina["nekretnina_id"] = nekretnina_id
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE nekretnina SET naziv=%(naziv)s, adresa=%(adresa)s, cena=%(cena)s WHERE id=%(nekretnina_id)s", nekretnina)
    db.commit()
    cursor.execute("SELECT * FROM nekretnina WHERE id=%s", (nekretnina_id,))
    nekretnina = cursor.fetchone()
    nekretnina["cena"] = float(nekretnina["cena"])
    return flask.jsonify(nekretnina)

@nekretnina_blueprint.route("<int:nekretnina_id>", methods=["delete"])
@is_admin()
def ukloni_nekretnina(nekretnina_id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM nekretnina WHERE id=%s", (nekretnina_id, ))
    db.commit()
    return ""

@nekretnina_blueprint.route("pretraga", endpoint='pretraga', methods=["POST"],)
@jwt_required()
def pretraga():
    objekat = flask.request.json
    tekst = "SELECT * FROM nekretnina WHERE "
    for key, value in objekat.items():
        tekst += "{}='{}' AND ".format(key, value)

    tekst = tekst[0:-4]
    cursor = mysql.get_db().cursor()
    cursor.execute(tekst)
    nekretnina = cursor.fetchall()
    for p in nekretnina:
        p["cena"] = float(p["cena"])
    print(nekretnina)
    return flask.jsonify(nekretnina)
