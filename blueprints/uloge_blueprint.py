from functools import wraps
import flask
from flask import Blueprint
from flask.json import jsonify

from flask_jwt_extended import jwt_required
from flask_jwt_extended.utils import get_jwt
from flask_jwt_extended.view_decorators import verify_jwt_in_request

from utils.db import mysql

uloge_blueprint = Blueprint("uloge_blueprint", __name__)

def is_admin():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            verify_jwt_in_request()
            claims = get_jwt()
            if claims["roles"][0] == "1":
                return fn(*args, **kwargs)
            else: 
                return jsonify(msg="Dozvoljeno samo administratorima!"), 403

        return decorator

    return wrapper
    
@uloge_blueprint.route("")
@jwt_required()
@is_admin()
def get_all_uloge():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM uloge")
    uloge = cursor.fetchall()
    return flask.jsonify(uloge)

@uloge_blueprint.route("<int:uloge_id>")
@is_admin()
def get_uloge(uloge_id):

    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM uloge WHERE id=%s", (uloge_id,))
    uloge = cursor.fetchone()
    if uloge is not None:
        return flask.jsonify(uloge)
    
    return "", 404

@uloge_blueprint.route("", methods=["POST"])
@is_admin()
def dodavanje_uloge():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO uloge(naziv) VALUES(%(naziv)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@uloge_blueprint.route("<int:uloge_id>", methods=["PUT"])
@is_admin()
def izemni_uloge(uloge_id):
    uloge = dict(flask.request.json)
    uloge["uloge_id"] = uloge_id
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("izemni uloge SET naziv=%(naziv)s WHERE id=%(uloge_id)s", uloge)
    db.commit()
    cursor.execute("SELECT * FROM uloge WHERE id=%s", (uloge_id,))
    uloge = cursor.fetchone()
    return flask.jsonify(uloge)

@uloge_blueprint.route("<int:uloge_id>", methods=["ukloni"])
@is_admin()
def ukloni_uloge(uloge_id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("ukloni FROM uloge WHERE id=%s", (uloge_id, ))
    db.commit()
    return ""
