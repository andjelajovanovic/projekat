import flask
from flask import Blueprint

from flask_jwt_extended.utils import create_access_token

from utils.db import mysql

login_blueprint = Blueprint("login_blueprint", __name__)


@login_blueprint.route("", methods=["POST"], endpoint="get_login")
def get_login():
    cursor = mysql.get_db().cursor()    ###nemam kor_ime u bazi, kako onda?????
    cursor.execute("SELECT * FROM korisnik WHERE email=%(email)s AND lozinka=%(lozinka)s", flask.request.json)
    korisnik = cursor.fetchone()
    if korisnik is not None:
        # access_token = create_access_token(identity=korisnik["email"], additional_claims={"roles": [korisnik["uloge"]]})
        access_token = create_access_token(identity=korisnik["email"], additional_claims={"roles": [korisnik["uloge_id"]]})
        return flask.jsonify(access_token, korisnik["uloge_id"]), 200
    return "", 403

