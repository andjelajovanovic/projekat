import flask
from flask import Flask
from flask import session
from flask_jwt_extended.utils import get_jwt

from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required

from flask_jwt_extended import JWTManager

from utils.db import mysql

from blueprints.korisnik_blueprint import korisnik_blueprint
from blueprints.nekretnina_blueprint import nekretnina_blueprint
from blueprints.kupovina_blueprint import kupovina_blueprint
from blueprints.uloge_blueprint import uloge_blueprint
from blueprints.login_blueprint import login_blueprint

app = Flask(__name__, static_url_path="/")
app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "707235vr"
app.config["MYSQL_DATABASE_DB"] = "projekat"
app.config["JWT_SECRET_KEY"] = "txYA.EC-fh5XT+?mM4t6"

app.register_blueprint(korisnik_blueprint, url_prefix="/api/korisnik")
app.register_blueprint(nekretnina_blueprint, url_prefix="/api/nekretnina")
app.register_blueprint(kupovina_blueprint, url_prefix="/api/kupovina")
app.register_blueprint(uloge_blueprint, url_prefix="/api/uloge")
app.register_blueprint(login_blueprint, url_prefix="/api/login")

mysql.init_app(app)

jwt = JWTManager(app)

@app.route("/")
def home():
    return app.send_static_file("index.html")

@app.route("/api/logout", methods=["GET"])
def logout():
    session.pop("korisnik", None)
    return "", 200
    
@app.route("/<path:path>")
def send_file(path):
    return app.send_static_file(path)

if __name__ == "__main__":
    app.run(debug=True)
    