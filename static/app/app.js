import TabelaKorisnik from './components/korisnik/tabelaKorisnik.js'
import TabelaNekretnina from './components/nekretnina/tabelaNekretnina.js'
import TabelaKupovina from './components/kupovina/tabelaKupovina.js'
import KorisnikForma from './components/korisnik/korisnikForma.js'
import KorisnikSearch from './components/korisnik/korisnikSearch.js'
import KupovinaSearch from './components/kupovina/kupovinaSearch.js'
import NekretninaSearch from './components/nekretnina/nekretninaSearch.js'
import KupovinaForma from './components/kupovina/kupovinaForma.js'
import NekretninaForma from './components/nekretnina/nekretninaForma.js'
import Korisnik from './components/korisnik/korisnik.js'
import Kupovina from './components/kupovina/kupovina.js'
import Nekretnina from './components/nekretnina/nekretnina.js'
import Login from './components/login/login.js'
import Agencija from './components/agencija.js'
import Logout from './components/logout/logout.js'

axios.interceptors.request.use(config => {
    let token = localStorage.getItem("token");
    Object.assign(config.headers, { "Authorization": `Bearer ${token}` });
    return config;
});

const router = VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    props: true,
    routes: [
        { path: "/", component: Login },
        { path: "/logout", component: Logout },
        { path: "/korisnik", component: Korisnik },
        { path: "/nekretnina", component: Nekretnina },
        { path: "/kupovina", component: Kupovina }
    ],
});

const app = Vue.createApp(Agencija);
app.component('tabela-korisnik', TabelaKorisnik);
app.component('tabela-nekretnina', TabelaNekretnina);
app.component('tabela-kupovina', TabelaKupovina);
app.component('korisnik-forma', KorisnikForma);
app.component('korisnik-search', KorisnikSearch);
app.component('kupovina-search', KupovinaSearch);
app.component('nekretnina-search', NekretninaSearch);
app.component('nekretnina-forma', NekretninaForma);
app.component('kupovina-forma', KupovinaForma);
app.use(router);
app.mount("#app");