export default {
    props: ["nekretnina"],
    emits: ["izmena", "uklanjanje"],
    data() {
        return {}
    },
    template: `
<table>
<thead>
    <tr>
        <th>ID</th>
        <th>naziv</th>
        <th>adresa</th>
        <th>cena</th>
    </tr>
</thead>
<tbody>
    <tr v-for="nek in nekretnina">
        <td>{{nek.id}}</td>
        <td>{{nek.naziv}}</td>
        <td>{{nek.adresa}}</td>
        <td>{{nek.cena}}</td>
        <td><button v-on:click="$emit('izmena', {...nek})">Izmeni</button><button
                v-on:click="$emit('uklanjanje', {...nek})">Ukloni</button></td>
    </tr>
</tbody>
</table>
    `
}