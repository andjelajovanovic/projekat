export default {
    props: ["nekretnina", "tekst"],
    emits: ["sacuvaj"],
    data() {
        return {
            noviNekretnina: this.nekretnina ? {...this.nekretnina} : {}
        }
    },
    watch: {
        nekretnina: function(newValue, oldValue) {
             this.noviNekretnina = {...this.nekretnina};
         }
    },
    template: `
    <form v-on:submit.prevent="$emit('sacuvaj', {...noviNekretnina})">
        <div>
            <label>naziv: <input type="text" v-model="noviNekretnina.naziv"></label>
        </div>
        <div>
            <label>adresa: <input type="text" v-model="noviNekretnina.adresa"></label>
        </div>
        <div>
            <label>cena: <input type="text" v-model="noviNekretnina.cena"></label>
        </div>
        <div>
            <input type="submit" v-bind:value="tekst">
        </div>
    </form>
    `
}