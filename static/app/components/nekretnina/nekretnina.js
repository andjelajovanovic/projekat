export default {
    template: `
<div>
    <nekretnina-forma v-on:sacuvaj="create" v-bind:tekst="'Dodaj'"></nekretnina-forma>
    <nekretnina-forma v-bind:nekretnina="nekretninaZaIzmenu" v-bind:tekst="'Izmeni'" v-on:sacuvaj="update"></nekretnina-forma>
    <nekretnina-search v-bind:nekretnina="nekretninaZaIzmenu" v-bind:tekst="'Search'" v-on:sacuvaj="pretraga"></nekretnina-search>
    <tabela-nekretnina v-bind:nekretnina="nekretnina" v-on:uklanjanje="remove" v-on:izmena="setnekretninaZaIzmenu"></tabela-nekretnina>
</div>
    `,
    data() {
        return {
            nekretnina: [],
            nekretninaZaIzmenu: {},
        }
    },
    methods: {
        setnekretninaZaIzmenu(nekretnina) {
            this.nekretninaZaIzmenu = { ...nekretnina };
        },
        refreshnekretnina() {
            axios.get("api/nekretnina").then((response) => {
                this.nekretnina = response.data;
            });
        },
        create(nekretnina) {
            axios.post("api/nekretnina", nekretnina).then((response) => {
                this.refreshnekretnina();
            });
        },
        update(nekretnina) {
            console.log(nekretnina);
            axios.put(`api/nekretnina/${nekretnina.id}`, nekretnina).then((response) => {
                this.refreshnekretnina();
            });
        },
        remove(nekretnina) {
            axios.delete(`api/nekretnina/${nekretnina.id}`).then((response) => {
                this.refreshnekretnina();
            });
        },

        pretraga(nekretnina) {
            axios.post(`api/nekretnina/pretraga`, nekretnina).then((response) => {
                this.nekretnina = response.data
            });
        }
    },
    created() {
        this.refreshnekretnina();
    }
}