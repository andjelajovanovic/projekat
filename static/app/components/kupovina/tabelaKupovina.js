export default {
    props: ["kupovina"],
    emits: ["izmena", "uklanjanje"],
    data() {
        return {}
    },
    
    template: `
    <table>
    <thead>
        <tr>
            <th>ID</th>
            <th>datum kupovine</th>
            <th>cena</th>
            <th>korisnik id</th>
            <th>nekretnina id</th>
            
        </tr>
    </thead>
    <tbody>
        <tr v-for="kup in kupovina">
            <td>{{kup.id}}</td>
            <td>{{kup.datum_kupovine}}</td>
            <td>{{kup.cena}}</td>
            <td>{{kup.korisnik_id}}</td>
            <td>{{kup.nekretnina_id}}</td>
            
            <td><button v-on:click="$emit('izmena', {...kup})">Izmeni</button><button
                    v-on:click="$emit('uklanjanje', {...kup})">Ukloni</button></td>
        </tr>
    </tbody>
    </table>
        `
}