export default {
    template: `
<div>
    <kupovina-forma v-on:sacuvaj="create" v-bind:korisnik="korisnik" v-bind:nekretnina="nekretnina" v-bind:tekst="'Dodaj'"></kupovina-forma>
    <kupovina-forma  v-bind:korisnik="korisnik" v-bind:nekretnina="nekretnina" v-bind:kupovina="kupovinaZaIzmenu" v-bind:tekst="'Izmeni'" v-on:sacuvaj="update"></kupovina-forma>
    <kupovina-search  v-bind:korisnik="korisnik" v-bind:nekretnina="nekretnina" v-bind:kupovina="kupovinaZaIzmenu" v-bind:tekst="'Search'" v-on:sacuvaj="pretraga"></kupovina-search>

    <tabela-kupovina v-bind:kupovina="kupovina" v-on:uklanjanje="remove" v-on:izmena="setkupovinaZaIzmenu"></tabela-kupovina>
</div>
    `,
    data() {
        return {
            kupovina: [],
            kupovinaZaIzmenu: {},
            korisnik: [],
            nekretnina: [],
        }
    },
    methods: {
        setkupovinaZaIzmenu(kupovina) {
            this.kupovinaZaIzmenu = { ...kupovina };
        },
        refreshkupovina() {
            axios.get("api/kupovina").then((response) => {
                this.korisnik = [response.data][0][1];
                this.nekretnina = [response.data][0][2];
                this.kupovina = [response.data][0][3];
            });
        },
        create(kupovina) {
            axios.post("api/kupovina", kupovina).then((response) => {
                this.refreshkupovina();
            });
        },
        update(kupovina) {
            console.log(kupovina);
            axios.put(`api/kupovina/${kupovina.id}`, kupovina).then((response) => {
                this.refreshkupovina();
            });
        },
        remove(id) {
            axios.delete(`api/kupovina/${id}`).then((response) => {
                this.refreshkupovina();
            });
        },

        pretraga(kupovina) {
            axios.post(`api/kupovina/pretraga`, kupovina).then((response) => {
                this.kupovina = response.data
            });
        }
    },
    created() {
        this.refreshkupovina();

        if(localStorage.getItem("uloge_id") != 1){
            document.getElementById("korisnik").remove()
            document.getElementById("nekretnine").remove()
        }
        
    }
}