export default {
    props: ["kupovina", "nekretnina", "korisnik", "tekst"],
    emits: ["sacuvaj"],
    data() {
        return {
            novaKupovina: this.kupovina ? {...this.kupovina} : {}
        }
    },
    watch: {
        kupovina: function(newValue, oldValue) {
             this.novaKupovina = {...this.kupovina};
         }
    },
   
    template: `
    <form v-on:submit.prevent="$emit('sacuvaj', {...novaKupovina})">
        <div>
            <label>Nekretnina:
                <select v-model="novaKupovina.nekretnina_id" >
                    <option v-for="nek in nekretnina" v-bind:value="nek.id">{{nek.naziv}} | {{nekretnina.adresa}} | {{nekretnina.cena}}</option>
                </select>
            </label>
        </div>
        <div>
            <label>Korisnik:
                <select v-model="novaKupovina.korisnik_id" >
                    <option v-for="kor in korisnik" v-bind:value="kor.id">{{kor.ime}} | {{kor.prezime}} | {{kor.email}} | {{kor.lozinka}}</option>
                </select>
            </label>
        </div>
        <div>
            <label>Datum kupovine: <input type="date" v-model="novaKupovina.datum_kupovine" ></label><span></span>
        </div>
        <div>
            <label>Cena: <input type="number" v-model="novaKupovina.cena" ></label>
        </div>
        <div>
            <input type="submit" v-bind:value="tekst">
        </div>
    </form>
    `
}