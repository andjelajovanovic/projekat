export default {
    template: `<button v-on:click="logOut()">Log out</button>`,
    methods: {
        logOut() {
            localStorage.removeItem("token");
            this.$router.push("/");
        }
    }
}