export default {
    template: `
<div>
    <korisnik-forma v-bind:uloga="uloga" v-on:sacuvaj="create" v-bind:tekst="'Dodaj'"></korisnik-forma>
    <korisnik-forma v-bind:uloga="uloga" v-bind:korisnik="korisnikZaIzmenu" v-bind:tekst="'Izmeni'" v-on:sacuvaj="update"></korisnik-forma>
    <korisnik-search v-bind:uloga="uloga" v-on:sacuvaj="pretraga" v-bind:tekst="'Search'"></korisnik-search>
    <tabela-korisnik v-bind:korisnik="korisnik" v-on:uklanjanje="remove" v-on:izmena="setkorisnikZaIzmenu"></tabela-korisnik>
</div>
    `,
    data() {
        return {
            korisnik: [],
            uloga: [],
            korisnikZaIzmenu: {},
        }
    },
    methods: {
        setkorisnikZaIzmenu(korisnik) {
            this.korisnikZaIzmenu = { ...korisnik };
        },
        refreshKorisnik() {
            axios.get(`api/korisnik`).then((response) => {
                this.korisnik = [response.data][0][1];
                this.uloga = [response.data][0][2];
            });
        },
        create(korisnik) {
            axios.post("api/korisnik", korisnik).then((response) => {
                this.refreshKorisnik();
            });
        },
        update(korisnik) {
            axios.put(`api/korisnik/${korisnik.id}`, korisnik).then((response) => {
                this.refreshKorisnik();
            });
        },
        remove(korisnik) {
            axios.delete(`api/korisnik/${korisnik.id}`).then((response) => {
                this.refreshKorisnik();
            });
        },
        pretraga(korisnik) {
            axios.post(`api/korisnik/pretraga`, korisnik).then((response) => {
                this.korisnik = response.data
            });
        }
    },
    created() {
        this.refreshKorisnik();
    }
}