export default {
    props: ["korisnik"],
    emits: ["izmena", "uklanjanje"],
    data() {
        return {}
    },
    template: `
<table>
<thead>
    <tr>
        <th>ID</th>
        <th>ime</th>
        <th>prezime</th>
        <th>email</th>
        <th>lozinka</th>
        <th>uloga</th>
    </tr>
</thead>
<tbody>
    <tr v-for="kor in korisnik">
        <td>{{kor.id}}</td>
        <td>{{kor.ime}}</td>
        <td>{{kor.prezime}}</td>
        <td>{{kor.email}}</td>
        <td>{{kor.lozinka}}</td>
        <td>{{kor.uloge_id}}</td>
        <td><button v-on:click="$emit('izmena', {...kor})">Izmeni</button><button
                v-on:click="$emit('uklanjanje', {...kor})">Ukloni</button></td>
    </tr>
</tbody>
</table>
    `
}