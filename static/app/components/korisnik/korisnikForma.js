export default {
    props: ["korisnik", "tekst", "uloga"],
    emits: ["sacuvaj"],
    data() {
        return {
            noviKorisnik: this.korisnik ? {...this.korisnik} : {}
        }
    },
    watch: {
         korisnik: function(newValue, oldValue) {
             this.noviKorisnik = {...this.korisnik};
         }
    },
    template: `
    <form v-on:submit.prevent="$emit('sacuvaj', {...noviKorisnik})">
        <div>
            <label>Ime: <input type="text" v-model="noviKorisnik.ime" required></label>
        </div>
        <div>
            <label>Prezime: <input type="text" v-model="noviKorisnik.prezime" required></label>
        </div>
        <div>
            <label>Email: <input type="text" v-model="noviKorisnik.email" required></label>
        </div>
        <div>
            <label>Lozinka: <input type="password" v-model="noviKorisnik.lozinka" required></label>
        </div>
        <select class="form-select" v-model="noviKorisnik['uloge_id']" required>
                    <option v-for="obj in uloga" v-bind:value="obj.id">{{obj.naziv}}</option>
                </select>
        <div>
            <input type="submit" v-bind:value="tekst">
        </div>
    </form>
    `
}